package Config;

public class API_URL_Config {
    public static String getAllProcessDataTest(getandset newObj){
        String getMessage="/api/bpm/runtime/process/instance/"+newObj.getProcessID()+"/nodes";
        return getMessage;
    }

    public static String getMeterDataTest(String meterNo){
        String getMessage="/api/ddr/devices/"+meterNo;
        return getMessage;
    }

    public static String getSpecificProcessDataTest(getandset newObj){
        String getMessage="/api/bpm/runtime/allprocesses";
        return getMessage;
    }

    public static String getAllCommunicationTaskResultTest(String deviceID){
        String getMessage="/api/ddr/devices/"+deviceID+"/comtasks";
        return getMessage;
    }
    public static String runSpecificProcessWithParametersTest(getandset newObj, String parameters){
        String postMessage="/flow/rest/runtime/"+newObj.getProcessDeploymentID()+"/withvars/process/"+newObj.getWorkflowID()+"/start/?"+parameters;
        return postMessage;
    }
}
