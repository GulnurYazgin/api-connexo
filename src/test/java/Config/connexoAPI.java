
package Config;

import io.restassured.RestAssured;
import io.restassured.http.ContentType;
import io.restassured.http.Header;
import io.restassured.response.Response;

import static io.restassured.RestAssured.given;

public class connexoAPI {

    public static Response response;

    public static Response getMessages(String message){
        response = given().
        header(new Header("Authorization", "Bearer " + getandset.Token)).
                        when().
                        get(message).
                        then().
                        extract().response();
        return response;
    }

    public static Response postMessages(String message){
        response = given().
        header(new Header("Authorization", "Bearer " +  getandset.Token)).
                        header("Accept", "application/json").
                        headers("Content-Type", ContentType.JSON, "Accept", ContentType.JSON).
                        when().
                        post(message).
                        then().extract().response();
        return response;
    }

}


