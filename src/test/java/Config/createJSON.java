package Config;

    public class createJSON {
        public static String runCommunicationTaskJSON(String processName, getandset newObj) {
            String jsonFormat ="{" +
                    "\"status\": \"null\"," +
                    "\"deploymentId\": \"" + newObj.getProcessDeploymentID()+"\"," +
                    "\"businessObject\": {" +
                    "\"type\": \"device\"," +
                    "\"id\": \"deviceId\"," +
                    "\"value\": \"" + newObj.getMeterMrid() + "\"" +
                    "}," +
                    "\"versionDB\": \"7\"," +
                    "\"processVersion\": \"1.0\"," +
                    "\"processName\": \"" + processName + "\"," +
                    "\"extraProperties\": []," +
                    "\"id\": \"" + newObj.getWorkflowID() + "\"," +
                    "\"properties\":  [" +
                    "{" +
                    "\"key\": \"InputText140103217\"," +
                    "\"name\": \"Communication task name\"," +
                    "\"required\": true," +
                    "\"propertyValueInfo\": {" +
                    "\"value\": \"" + newObj.getCommunicationTaskName() + "\"," +
                    "\"defaultValue\": \"\"," +
                    "\"inheritedValue\": \"\"" +
                    "}," +
                    "\"propertyTypeInfo\": {" +
                    "\"simplePropertyType\": \"TEXT\"" +
                    "}" +
                    "}," +
                    "{" +
                    "\"key\": \"InputTextInteger1611606575\"," +
                    "\"name\": \"Estimated duration of com task execution in minutes\"," +
                    "\"required\": false," +
                    "\"propertyValueInfo\": {" +
                    "\"value\": 4," +
                    "\"defaultValue\": \"5\"," +
                    "\"inheritedValue\": \"\"" +
                    "}," +
                    "\"propertyTypeInfo\": {" +
                    "\"simplePropertyType\": \"LONG\"" +
                    "}" +
                    "}," +
                    "{" +
                    "\"key\": \"InputText689548321\"," +
                    "\"name\": \"Max duration of check result subworkflow (format #d#h#m#s)\"," +
                    "\"required\": false," +
                    "\"propertyValueInfo\": {" +
                    "\"value\": \"5m\"," +
                    "\"defaultValue\": \"24h\"," +
                    "\"inheritedValue\": \"\"" +
                    "}," +
                    "\"propertyTypeInfo\": {" +
                    "\"simplePropertyType\": \"TEXT\"" +
                    "}" +
                    "}," +
                    "{" +
                    "\"key\": \"CheckBox244675122\"," +
                    "\"name\": \"No retries needed\"," +
                    "\"required\": false," +
                    "\"propertyValueInfo\": {" +
                    "\"value\": " + newObj.getNoRetriesNeeded() + "," +
                    "\"defaultValue\": \"\"," +
                    "\"inheritedValue\": \"\"" +
                    "}," +
                    "\"propertyTypeInfo\": {" +
                    "\"simplePropertyType\": \"BOOLEAN\"" +
                    "}" +
                    "}," +
                    "{" +
                    "\"key\": \"InputText1769229804\"," +
                    "\"name\": \"Tracking id\"," +
                    "\"required\": false," +
                    "\"propertyValueInfo\": {" +
                    "\"value\": \"\"," +
                    "\"defaultValue\": \"\"," +
                    "\"inheritedValue\": \"\"" +
                    "}," +
                    "\"propertyTypeInfo\": {" +
                    "\"simplePropertyType\": \"TEXT\"" +
                    "}}]}";
            System.out.println("here is the json \n" +jsonFormat);
            return jsonFormat;
        }
    }