package Config;

public class getandset {
    public static String Token = "eyJhbGciOiJSUzI1NiJ9.eyJwcml2aWxlZ2VzIjpbInByaXZpbGVnZS5wdWJsaWMuYXBpLnJlc3QiLCJwcml2aWxlZ2UucHVsc2UucHVibGljLmFwaS5yZXN0IiwicHJpdmlsZWdlLnZpZXcudXNlckFuZFJvbGUiLCJwcml2aWxlZ2UuZGVzaWduLmJwbSIsInByaXZpbGVnZS5kZXNpZ24ucmVwb3J0cyIsInByaXZpbGVnZS5kZXNpZ24ucmVwb3J0cyIsInByaXZpbGVnZS5hZG1pbmlzdHJhdGUucmVwb3J0cyIsInByaXZpbGVnZS5kZXNpZ24uYnBtIl0sInN1YiI6IjIiLCJyb2xlcyI6W3siaWQiOjEsIm5hbWUiOiJCYXRjaCBleGVjdXRvciJ9LHsiaWQiOjMsIm5hbWUiOiJJbnN0YWxsZXIifSx7ImlkIjo0LCJuYW1lIjoiU3lzdGVtIGFkbWluaXN0cmF0b3IifSx7ImlkIjo2LCJuYW1lIjoiQnVzaW5lc3MgcHJvY2VzcyBkZXNpZ25lciJ9LHsiaWQiOjcsIm5hbWUiOiJCdXNpbmVzcyBwcm9jZXNzIGV4ZWN1dG9yIn0seyJpZCI6OCwibmFtZSI6IkRldmVsb3BlciJ9LHsiaWQiOjksIm5hbWUiOiJSZXBvcnQgZGVzaWduZXIifSx7ImlkIjoxMCwibmFtZSI6IlJlcG9ydCBhZG1pbmlzdHJhdG9yIn0seyJpZCI6MTEsIm5hbWUiOiJEYXRhIGV4cGVydCJ9LHsiaWQiOjEyLCJuYW1lIjoiRGF0YSBvcGVyYXRvciJ9LHsiaWQiOjE0LCJuYW1lIjoiTWV0ZXIgZXhwZXJ0In0seyJpZCI6MTUsIm5hbWUiOiJNZXRlciBvcGVyYXRvciJ9LHsiaWQiOjE2LCJuYW1lIjoiUmVwb3J0IHZpZXdlciJ9LHsiaWQiOjEwMDEsIm5hbWUiOiJEZWZhdWx0IFJvbGUifV0sImNudCI6MCwiaXNzIjoiRWxzdGVyIENvbm5leG8iLCJleHAiOjkyMjMzNzIwMzY4NTQ3NzUsImlhdCI6MTU5MTYxNzcwNywianRpIjoiZWQxMDlkNDMtYWJjYy00NmRjLTgxMGYtZDZhOGI2N2MxODJhIiwidXNlcm5hbWUiOiJwcm9jZXNzIGV4ZWN1dG9yIn0.frMgZRsZ5ORQSEDehvzhiGWq04cqh2Kbr_ppb7CDs60t8CQFOTsrWqjz-ToK2qX6jna_cfWrEGMp7jfQmP9VeHFttpfRu2U8tTRyNNAt4DftjOYqMW6HiC-IXereCN7vKrFLcXGiC5BjMhqe43MNWXnyjHfCXYDUpa_SSywZLmw  ";    public String processID;
    public String workflowID;
    public String processDeploymentID;
    public String meterNo;
    public String meterMrid;
    public String processName;
    public String communicationTaskName;
    public String estimatedDurationOfComTaskExecutionInMinutes;
    public String maxDurationOfCheckResultSubWorkflow;
    public Boolean noRetriesNeeded;
    public String administrativeStatus;
    public Boolean addressable;
    public String securityAccessor;
    public static Boolean logSwitch = true;

    public String getProcessID(){
        return processID;
    }
    public String getWorkflowID(){
        return workflowID;
    }
    public String getProcessDeploymentID(){
        return processDeploymentID;
    }
    public String getMeterNo(){
        return meterNo;
    }
    public String getMeterMrid(){
        return meterMrid;
    }
    public String getProcessName(){
        return processName;
    }
    public String getCommunicationTaskName(){
        return communicationTaskName;
    }
    public String getEstimatedDurationOfComTaskExecutionInMinutes(){
        return estimatedDurationOfComTaskExecutionInMinutes;
    }
    public String getMaxDurationOfCheckResultSubWorkflow(){
        return maxDurationOfCheckResultSubWorkflow;
    }
    public Boolean getNoRetriesNeeded(){
        return noRetriesNeeded;
    }
    public String getAdministrativeStatus(){
        return administrativeStatus;
    }
    public Boolean getAdressable(){
        return addressable;
    }
    public String getSecurityAccessor(){
        return securityAccessor;
    }


    public void setProcessID(String data){
        this.processID= data;
    }
    public void setWorkflowID(String data){
        this.workflowID= data;
    }
    public void setProcessDeploymentID(String data){
        this.processDeploymentID= data;
    }
    public void setMeterNo(String data){
        this.meterNo= data;
    }
    public void setMeterMrid(String data){
        this.meterMrid= data;
    }
    public void setProcessName(String data){
        this.processName= data;
    }
    public void setCommunicationTaskName(String data){
        this.communicationTaskName= data;
    }
    public void setEstimatedDurationOfComTaskExecutionInMinutes(String data){
        this.estimatedDurationOfComTaskExecutionInMinutes= data;
    }
    public void setMaxDurationOfCheckResultSubWorkflow(String data){
        this.maxDurationOfCheckResultSubWorkflow= data;
    }
    public void setNoRetriesNeeded(Boolean data){
        this.noRetriesNeeded= data;
    }
    public void setAdministrativeStatus(String data){
        this.administrativeStatus= data;
    }
    public void setAdressable(Boolean data){
        this.addressable= data;
    }
    public void setSecurityAccessor(String data){
        this.securityAccessor= data;
    }




}

