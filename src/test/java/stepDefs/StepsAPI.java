package stepDefs;

import Config.API_URL_Config;
import Config.connexoAPI;
import Config.getandset;
import cucumber.api.DataTable;
import cucumber.api.java.Before;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import io.restassured.RestAssured;
import io.restassured.filter.log.RequestLoggingFilter;
import io.restassured.filter.log.ResponseLoggingFilter;

import java.io.IOException;
import static org.junit.Assert.assertEquals;

public class StepsAPI {
    getandset newObj = new getandset();


        @Before
    public void setup(){
        RestAssured.baseURI = "https://enexxo-tst.enexis.nl";
        RestAssured.basePath = "";
        if(getandset.logSwitch== true) {
            RestAssured.filters(new RequestLoggingFilter(), new ResponseLoggingFilter());
        }
        RestAssured.useRelaxedHTTPSValidation();
    }

    @Given("^user creates an API request to start workflow \"([^\"]*)\" with parameters$")
    public void user_creates_an_API_request_to_start_workflow(String processName, DataTable content) throws IOException {
        System.out.println("the process name is " +processName);
        connexoAPI.getMessages(API_URL_Config.getSpecificProcessDataTest(newObj));
        genericFunctions.getSpecificProcessDataValidation(processName,newObj,connexoAPI.response);
        parameterFunctions.assignParameterValues(processName,content,newObj);
        connexoAPI.getMessages(API_URL_Config.getMeterDataTest(newObj.getMeterNo()));
        genericFunctions.getMeterDataTestValidation(newObj,connexoAPI.response);//to print meter mrid
        String parameters=parameterFunctions.determineWorkflowParameters(processName, newObj);
        connexoAPI.postMessages(API_URL_Config.runSpecificProcessWithParametersTest(newObj,parameters));
        genericFunctions.runSpecificProcessWithParametersTestValidation(newObj,connexoAPI.response);
    }

    @When("^user verifies if Process \"([^\"]*)\" is completed and OK$")
    public void user_verifies_if_Process_is_completed_and_OK(String processName) throws Exception {
        String processEndResult= genericFunctions.makeSureTheProcessIsNotActive(newObj,connexoAPI.response); //should be Completed
        //we want to check every node of hte latest json response and make sure all OK's have value of true
        //get the response of the saved process id.
        assertEquals(processEndResult, "Completed");
        genericFunctions.makeSureTheProcessIsOK(connexoAPI.response);
    }

    @Then("^user verifies if communication task \"([^\"]*)\" is completed and has result \"([^\"]*)\" for \"([^\"]*)\"$")
    public void user_verifies_if_communication_task_is_completed_and_has_result_for(String comTask, String Result, String deviceID) throws Exception {
        connexoAPI.getMessages(API_URL_Config.getAllCommunicationTaskResultTest(deviceID));
        genericFunctions.getAllCommunicationTaskResultTestValidation(comTask,Result,connexoAPI.response);
    }
}
