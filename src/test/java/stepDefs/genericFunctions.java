package stepDefs;

import Config.API_URL_Config;
import Config.connexoAPI;
import Config.getandset;
import cucumber.api.DataTable;
import io.restassured.path.json.JsonPath;
import io.restassured.response.Response;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import static org.junit.Assert.assertEquals;

public class genericFunctions {
    public static int createListOfValues(String searchCriteria, String searchFor, Response response) {
        List<String> listOfValues = response.path(searchFor);
        int nrRow = listOfValues.indexOf(searchCriteria);
        System.out.println("the index is :" +nrRow);
        return nrRow;
    }

    public static void assignValuesToParameters(DataTable content, getandset newObj) throws IOException {
        List<Map<String, String>> list = content.asMaps(String.class, String.class);
        for (Map<String, String> stringStringMap : list) {
            String value = stringStringMap.get("value");
            switch (stringStringMap.get("parameter")) {
                case "Device name":
                    newObj.setMeterNo(stringStringMap.get(value));
                    System.out.println("the device no is " + newObj.getMeterNo());
            }
            break;
        }
    }

    public static void getAllCommunicationTaskResultTestValidation(String comTask,String Result,Response response) throws InterruptedException {
        //Gets all the results of a specific communication task and checks if it matches the required result
        int index= genericFunctions.createListOfValues(comTask,"comTasks.comTask.name",response);
        JsonPath jsonPathEvaluator = response.jsonPath();
        assertEquals( jsonPathEvaluator.get("comTasks["+index+"].latestResult.displayValue"),Result);
    }

    public static void getMeterDataTestValidation(getandset newObj, Response response) {
        newObj.setMeterMrid(response.path("mRID"));
        System.out.println("the mrid of the meter is " +newObj.getMeterMrid()); //debug purposes
    }

    public static void getSpecificProcessDataValidation(String processName,getandset newObj,Response response) {
        int nrRow= genericFunctions.createListOfValues(processName,"processes.name",response);
        //Retrieve a specific information out of the whole created list
        newObj.setWorkflowID(response.path("processes.processId["+nrRow+"]"));
        newObj.setProcessDeploymentID(response.path("processes.deploymentId["+nrRow+"]"));
        System.out.println("the workflow id is: " +newObj.getWorkflowID());
        System.out.println("the process deployment id is: " +newObj.getProcessDeploymentID());
    }
    public static void runSpecificProcessWithParametersTestValidation(getandset newObj,Response response) {
        //Creates a process based on the parameters provided
        //Sets the process ID in the getandset
        System.out.println((Integer) response.path("processInstance.id"));//debug purposes
        newObj.setProcessID(Integer.toString(response.path("processInstance.id")));
        System.out.println("process id is: " +newObj.getProcessID());
    }


    //make generic later
    public static String makeSureTheProcessIsNotActive(getandset newObj,Response response) throws InterruptedException {
        //Checks the status of the process and waits until its NOT Active.
        //returns the last status as a string
        boolean check=true;
        while (check) {
            Response responseGetAllProcessDataTest = connexoAPI.getMessages(API_URL_Config.getAllProcessDataTest(newObj));
            JsonPath jsonPathEvaluator = responseGetAllProcessDataTest.jsonPath();
            String status = jsonPathEvaluator.get("processInstanceStatus");
            System.out.println("status is  "+status);
            if (status.equals("Active")) {
                System.out.println("status is still active, waiting for 30sec...");
                Thread.sleep(30000);//bunu 1000 yap
            } else {
                System.out.println("status is not active, moving further...");
                check = false;
            }
        }
        Response responseGetAllProcessDataTest2 = connexoAPI.getMessages(API_URL_Config.getAllProcessDataTest(newObj));
        JsonPath jsonPathEvaluator2 = responseGetAllProcessDataTest2.jsonPath();
        String statusLast = jsonPathEvaluator2.get("processInstanceStatus");
        System.out.println("status is " + statusLast);
        //then we can check if its completed, aborted, cancelled
        return statusLast;
    }

    //make generic later
    public static void makeSureTheProcessIsOK(Response response) throws InterruptedException {
        //here please build loops to check if all ok values are true
        //check the size of the nodes and loop for all elements  processInstanceNodes
        //check the size of the first processInstanceVariables
        //check the vriableName Ok to be true and ignore the rest
        //if ok is not true use function fail("");
        JsonPath jsonPathEvaluator = response.jsonPath();
        ArrayList<String> allProcessNodes = jsonPathEvaluator.get("processInstanceNodes");
        int sizeOfArray=allProcessNodes.size();
        System.out.println("the size is" +sizeOfArray);
        //loop through the instance nodes
        for(int i=0; i<sizeOfArray; i++){
            ArrayList<String> allInstanceVariables = jsonPathEvaluator.get("processInstanceNodes.processInstanceVariables["+i+"]");
            String currentInstanceName=jsonPathEvaluator.get("processInstanceNodes["+i+"].name");
            int sizeOfInstanceVariables=allInstanceVariables.size();
            System.out.println("the size of instance variable "+i+" is " +sizeOfInstanceVariables);
            //loop through the instance variables if it has more than 0 elements
            if(sizeOfInstanceVariables!=0) {
                System.out.println("the current instance name is "+currentInstanceName);
                //this line is to exclude OK false scenarios
                if(!currentInstanceName.contains("configuration attribute")&&!currentInstanceName.contains("process variables")&&!currentInstanceName.contains("Get parameter values")&&!currentInstanceName.contains("Read register data")&&!currentInstanceName.contains("Check status command")&&!currentInstanceName.contains("Check status command")&&!currentInstanceName.contains("Get all \"Weekly\" shared schedules")){
                    for (int j = 0; j < sizeOfInstanceVariables; j++) {
                        String valueName = jsonPathEvaluator.get("processInstanceNodes.processInstanceVariables[" + i + "].variableName[" + j + "]");
                        String value = jsonPathEvaluator.get("processInstanceNodes.processInstanceVariables[" + i + "].value[" + j + "]");
                        if (valueName.equals("OK")) {
                            //check if the value is true. if not print a line
                            assertEquals(value, "true");
                        }
                    }
                }
            }
        }
    }
}




