package stepDefs;

import Config.API_URL_Config;
import Config.connexoAPI;
import Config.getandset;
import cucumber.api.DataTable;

import java.io.IOException;
import java.util.List;
import java.util.Map;

public class parameterFunctions {

        public static void assignParameterValues(String workflowName, DataTable content, getandset newObj) throws IOException {
        List<Map<String, String>> list = content.asMaps(String.class, String.class);
        for (Map<String, String> columns : list) {
//            System.out.println("titles are "+columns.get("parameter"));
            switch (columns.get("parameter")) {
                case "Device name":
                    newObj.setMeterNo(columns.get("value"));
                    System.out.println("the device no is " + newObj.getMeterNo());
                    break;
                case "Communication task name":
                    newObj.setCommunicationTaskName(columns.get("value"));
                    System.out.println("Communication task name " + newObj.getCommunicationTaskName());
                    break;
                case "Estimated duration of com task execution in minutes":
                    newObj.setEstimatedDurationOfComTaskExecutionInMinutes(columns.get("value"));
                    System.out.println("Estimated duration of com task execution in minutes " + newObj.getEstimatedDurationOfComTaskExecutionInMinutes());
                    break;
                case "Max duration of check result sub workflow":
                    newObj.setMaxDurationOfCheckResultSubWorkflow(columns.get("value"));
                    System.out.println("Max duration of check result sub workflow " + newObj.getMaxDurationOfCheckResultSubWorkflow());
                    break;
                case "No retries needed":
                    newObj.setNoRetriesNeeded(Boolean.parseBoolean(columns.get("value")));
                    System.out.println("No retries needed " + newObj.getNoRetriesNeeded());
                    break;
                case "Administrative Status":
                    newObj.setAdministrativeStatus(columns.get("value"));
                    System.out.println("Administrative Status " + newObj.getAdministrativeStatus());
                    break;
                case "Addressable":
                    newObj.setAdressable(Boolean.parseBoolean(columns.get("value")));
                    System.out.println("Is device Adressable " + newObj.getAdressable());
                    break;
                case "Security accessor":
                    newObj.setSecurityAccessor(columns.get("value"));
                    System.out.println("The Security acessor is " + newObj.getSecurityAccessor());
                    break;
            }
        }
    }
    //get the get and set as parameter
    public static String determineWorkflowParameters(String workflowName, getandset newObj) throws IOException {
//        setParameterValues(workflowName,content); //meter no etc is set here
        StringBuilder parameters = new StringBuilder(); //base of the resource path is built here

        connexoAPI.getMessages(API_URL_Config.getMeterDataTest(newObj.getMeterNo())); //mrid is set here

        switch (workflowName) {
            case "Run communication task workflow":
                // Required parameters
                parameters.append("map_deviceId=" + newObj.getMeterMrid());
                parameters.append("&").append("map_comTaskName=" + newObj.getCommunicationTaskName());
                //Optional parameters
                if (newObj.getEstimatedDurationOfComTaskExecutionInMinutes() != null)
                    parameters.append("&").append("map_estimatedDuration="+newObj.getEstimatedDurationOfComTaskExecutionInMinutes());
                if (newObj.getMaxDurationOfCheckResultSubWorkflow() != null)
                    parameters.append("&").append("map_maxDuration="+newObj.getMaxDurationOfCheckResultSubWorkflow());
                if (newObj.getNoRetriesNeeded() != null)
                    parameters.append("&").append("map_noRetryNeeded="+newObj.getNoRetriesNeeded());
                break;
            case "Send Calendar":
                // Required parameters
                parameters.append("map_deviceId=" + newObj.getMeterMrid());
                // Optional parameters
                if (newObj.getEstimatedDurationOfComTaskExecutionInMinutes() != null)
                    parameters.append("&").append("map_estimatedDuration="+newObj.getEstimatedDurationOfComTaskExecutionInMinutes());
                if (newObj.getMaxDurationOfCheckResultSubWorkflow() != null)
                    parameters.append("&").append("map_maxDuration="+newObj.getMaxDurationOfCheckResultSubWorkflow());
                break;
            case "Send administrative status":
                // Required parameters
                parameters.append("map_deviceId=" + newObj.getMeterMrid());
                // Optional parameters
                if (newObj.getEstimatedDurationOfComTaskExecutionInMinutes() != null)
                    parameters.append("&").append("map_estimatedDuration="+newObj.getEstimatedDurationOfComTaskExecutionInMinutes());
                if (newObj.getMaxDurationOfCheckResultSubWorkflow() != null)
                    parameters.append("&").append("map_maxDuration="+newObj.getMaxDurationOfCheckResultSubWorkflow());
                if (newObj.getAdministrativeStatus() != null)
                    parameters.append("&").append("map_administrativeStatus="+newObj.getAdministrativeStatus());
                break;
            case "Main key renewal":
            case "Configure Device":
            case "Device Main Deployment Workflow":
                // Required parameters
                parameters.append("map_deviceId=" + newObj.getMeterMrid());
                // Optional parameters
                if (newObj.getEstimatedDurationOfComTaskExecutionInMinutes() != null)
                    parameters.append("&").append("map_estimatedDuration="+newObj.getEstimatedDurationOfComTaskExecutionInMinutes());
                if (newObj.getMaxDurationOfCheckResultSubWorkflow() != null)
                    parameters.append("&").append("map_maxDuration="+newObj.getMaxDurationOfCheckResultSubWorkflow());
                if (newObj.getAdressable() != null)
                    parameters.append("&").append("map_addressable="+newObj.getAdressable());
                if (newObj.getNoRetriesNeeded() != null)
                    parameters.append("&").append("map_noRetryNeeded="+newObj.getNoRetriesNeeded());
                break;
            case "ActivateCommunicationTasks":
                // Required parameters
                parameters.append("map_deviceId=" + newObj.getMeterMrid());
                // Optional parameters
                if (newObj.getAdressable() != null)
                    parameters.append("&").append("map_addressable="+newObj.getAdressable());
                break;
            case "Determine Allocation Method":
            case "Stop Device":
                // Required parameters
                parameters.append("map_deviceId=" + newObj.getMeterMrid());
                break;
            case "Key renewal":
                // Required parameters
                parameters.append("map_deviceId=" + newObj.getMeterMrid());
                // Optional parameters
                if (newObj.getEstimatedDurationOfComTaskExecutionInMinutes() != null)
                    parameters.append("&").append("map_estimatedDuration="+newObj.getEstimatedDurationOfComTaskExecutionInMinutes());
                if (newObj.getMaxDurationOfCheckResultSubWorkflow() != null)
                    parameters.append("&").append("map_maxDuration="+newObj.getMaxDurationOfCheckResultSubWorkflow());
                if (newObj.getAdressable() != null)
                    parameters.append("&").append("map_addressable="+newObj.getAdressable());
                if (newObj.getNoRetriesNeeded() != null)
                    parameters.append("&").append("map_noRetryNeeded="+newObj.getNoRetriesNeeded());
                if (newObj.getSecurityAccessor() != null)
                    parameters.append("&").append("map_accessorType="+newObj.getSecurityAccessor());
                break;
        }
        System.out.println("Created string is" +parameters);
//        map_deviceId=d058a1e3-642c-409d-89d0-cfd651b22188&map_comTaskName=Ad Hoc Force Clock E
//        ?map_comTaskName=Ad Hoc Force Clock E&map_deviceId=d058a1e3-642c-409d-89d0-cfd651b22188").
        return parameters.toString();
    }
}