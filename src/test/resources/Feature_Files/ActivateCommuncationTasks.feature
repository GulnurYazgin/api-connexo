@Run
Feature: Activate Communication Tasks Workflow
  Scenario Outline: 01 - Start Activate Communication Tasks Workflow  for <deviceName>
    Given user creates an API request to start workflow "ActivateCommunicationTasks" with parameters
      | parameter                                           | value        |
      | Device name                                         | <deviceName> |
      | Addressable                                         | true         |
    When user verifies if Process "ActivateCommunicationTasks" is completed and OK
    Examples:
      | deviceName        |
      | SIM_E000000019619 |