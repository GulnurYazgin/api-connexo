@Run
Feature: Configure Device Workflow
  Scenario Outline: 01 - Start Configure Device workflow  for <deviceName>
    Given user creates an API request to start workflow "Configure Device" with parameters
      | parameter                                           | value        |
      | Device name                                         | <deviceName> |
      | Addressable                                         | true         |
      | Estimated duration of com task execution in minutes | 2            |
      | Max duration of check result sub workflow           | 5m           |
      | No retries needed                                   | false        |
    When user verifies if Process "Configure Device" is completed and OK
    Examples:
      | deviceName        |
      | SIM_E000000019619 |