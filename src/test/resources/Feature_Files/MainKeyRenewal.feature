@Run
Feature: Main Key Renewal Workflow
  Scenario Outline: 01 - Start Main Key Renewal workflow  for <deviceName>
    Given user creates an API request to start workflow "Main key renewal" with parameters
      | parameter                                           | value        |
      | Device name                                         | <deviceName> |
      | Addressable                                         | true         |
      | Estimated duration of com task execution in minutes | 2            |
      | Max duration of check result sub workflow           | 5m           |
      | No retries needed                                   | false        |
    When user verifies if Process "Main key renewal" is completed and OK
    Examples:
      | deviceName        |
      | SIM_E000000019619 |