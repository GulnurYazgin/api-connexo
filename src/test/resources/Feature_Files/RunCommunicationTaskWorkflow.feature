@Run
Feature: Run Communication Task Workflow
  Scenario Outline: 01 - Start Run communication task workflow  for <deviceName>
    Given user creates an API request to start workflow "Run communication task workflow" with parameters
      | parameter                                           | value               |
      | Device name                                         | <deviceName>        |
      | Communication task name                             | <communicationTask> |
      | Estimated duration of com task execution in minutes | 2                   |
      | Max duration of check result sub workflow           | 5m                  |
      | No retries needed                                   | false               |
    When user verifies if Process "Run communication task workflow" is completed and OK
    Then user verifies if communication task "<communicationTask>" is completed and has result "Successful" for "<deviceName>"
    Examples:
      | deviceName        | communicationTask                                  |
#      | SIM_E000000000119 | Ad Hoc Force Clock E                               |
      | SIM_E000000019619 | Ad Hoc Force Clock E                               |
#      | SIM_E000000019719 | Ad Hoc Force Clock E                               |
#      | SIM_E000000019619 | Ad Hoc Events E                                    |
#      | SIM_E000000019619 | Firmware management                                |
#      | SIM_E000000019619 | Ad Hoc Commands E                                  |
#      | SIM_E000000019619 | Ad Hoc Communication Check E                       |
#      | SIM_E000000019619 | Ad Hoc Justification Light Daily E                 |
#      | SIM_E000000019619 | Ad Hoc Justification Light Monthly E               |
#      | SIM_E000000019619 | Ad Hoc PSA E                                       |
#      | SIM_E000000019619 | Ad Hoc Read DLP (voltages) E                       |
#      | SIM_E000000019619 | Ad Hoc Read Registers Configuration E              |
#      | SIM_E000000019619 | Ad Hoc Read Registers Consumption E                |
#      | SIM_E000000019619 | Ad Hoc Read Registers Current E                    |
#      | SIM_E000000019619 | Ad Hoc Read Registers LTE Diagnostics Statistics E |
#      | SIM_E000000019619 | Ad Hoc Read Registers M-Bus Configuration E        |
#      | SIM_E000000019619 | Ad Hoc Read Registers Voltage E                    |
#      | SIM_E000000019619 | Ad Hoc Topology E                                  |
#      | SIM_E000000019619 | Ad HocPr Commands E                                |
#      | SIM_E000000019619 | Ad HocPr Read Registers APN E                      |
#      | SIM_E000000019619 | Ad HocPr Read Registers DLP Configuration E        |
#      | SIM_E000000019619 | Mass Commands E                                    |
#      | SIM_E000000019619 | Mass Communication check E                         |
#      | SIM_E000000019619 | Mass Read Registers APN E                          |
#      | SIM_E000000019619 | Mass Read Registers Configuration E                |
#      | SIM_E000000019619 | Mass Read Registers DLP Configuration E            |
#      | SIM_E000000019619 | Scheduled Read LTE Monitoring E                    |
#      | SIM_E000000019619 | Scheduled Read Registers Sags and Swells E         |
#      | SIM_E000000019619 | Scheduled Standard Data Request E                  |
#      | SIM_E000000019619 | Scheduled Standard E                               |
#      | SIM_E000000019619 | Scheduled Standard Evaluation E                    |
#      | SIM_E000000019619 | Ad Hoc Topology E (old)                            |

