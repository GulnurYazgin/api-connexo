@Run
Feature: Send Administrative Status Workflow
  Scenario Outline: 01 - Start Send administrative status workflow for <deviceName>
    Given user creates an API request to start workflow "Send administrative status" with parameters
      | parameter                                           | value        |
      | Device name                                         | <deviceName> |
      | Administrative Status                               | AAN          |
      | Estimated duration of com task execution in minutes | 2            |
      | Max duration of check result sub workflow           | 5m           |
    When user verifies if Process "Send administrative status" is completed and OK
    Examples:
      | deviceName        |
      | SIM_E000000019619 |
