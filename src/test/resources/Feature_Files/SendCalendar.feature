@Run
Feature: Send Calendar Workflow
  Scenario Outline: 01 - Start Send Calendar workflow for <deviceName>
    Given user creates an API request to start workflow "Send Calendar" with parameters
      | parameter                                           | value               |
      | Device name                                         | <deviceName>        |
      | Estimated duration of com task execution in minutes | 2                   |
      | Max duration of check result sub workflow           | 5m                  |
    When user verifies if Process "Send Calendar" is completed and OK
    Examples:
      | deviceName        |
      | SIM_E000000019619 |
