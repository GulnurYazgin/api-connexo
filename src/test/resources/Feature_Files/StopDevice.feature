@Run
Feature: Stop Device Workflow
  Scenario Outline: 01 - Start Stop Device workflow  for <deviceName>
    Given user creates an API request to start workflow "Stop Device" with parameters
      | parameter                                           | value        |
      | Device name                                         | <deviceName> |
    When user verifies if Process "Stop Device" is completed and OK
    Examples:
      | deviceName        |
      | SIM_E000000019619 |