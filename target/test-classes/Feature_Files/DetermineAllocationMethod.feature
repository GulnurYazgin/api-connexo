@Run
Feature: Configure Device Workflow
  Scenario Outline: 01 - Start Configure Device workflow  for <deviceName>
    Given user creates an API request to start workflow "Determine Allocation Method" with parameters
      | parameter                                           | value        |
      | Device name                                         | <deviceName> |
    When user verifies if Process "Determine Allocation Method" is completed and OK
    Examples:
      | deviceName        |
      | SIM_E000000019619 |