@Run
Feature:Device Main Deployment Workflow Workflow
  Scenario Outline: 01 - Start Device Main Deployment Workflow workflow  for <deviceName>
    Given user creates an API request to start workflow "Device Main Deployment Workflow" with parameters
      | parameter                                           | value        |
      | Device name                                         | <deviceName> |
      | Estimated duration of com task execution in minutes | 2            |
      | Max duration of check result sub workflow           | 5m           |
      | No retries needed                                   | false        |
    When user verifies if Process "Device Main Deployment Workflow" is completed and OK
    Examples:
      | deviceName        |
      | SIM_E000000019619 |